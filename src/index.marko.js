// Compiled using marko@4.15.1 - DO NOT EDIT
"use strict";

var marko_template = module.exports = require("marko/src/html").t(__filename),
    marko_componentType = "/fuzzy-node-template$1.0.0/src/index.marko",
    components_helpers = require("marko/src/components/helpers"),
    marko_renderer = components_helpers.r,
    marko_defineComponent = components_helpers.c,
    marko_loadTemplate = require("marko/src/runtime/helper-loadTemplate"),
    site_header_template = marko_loadTemplate(require.resolve("./components/site-header")),
    marko_helpers = require("marko/src/runtime/html/helpers"),
    marko_loadTag = marko_helpers.t,
    site_header_tag = marko_loadTag(site_header_template),
    click_count_template = marko_loadTemplate(require.resolve("./components/click-count")),
    click_count_tag = marko_loadTag(click_count_template),
    site_layout_template = marko_loadTemplate(require.resolve("./components/site-layout")),
    site_layout_tag = marko_loadTag(site_layout_template);

function render(input, out, __component, component, state) {
  var data = input;

  site_layout_tag({
      title: {
          renderBody: function renderBody(out) {
            out.w("Fuzzy Logic Starter");
          }
        },
      content: {
          renderBody: function renderBody(out) {
            site_header_tag({}, out, __component, "3");

            out.w("<div class=\"container\"><div class=\"px-4 my-4\"><div class=\"flex flex-wrap -mx-4\"><div class=\"w-full md:w-1/2 px-4 mb-10\"><div class=\"max-w-sm rounded overflow-hidden shadow-lg mx-auto\"><img class=\"w-full\" src=\"https://tailwindcss.com/img/card-top.jpg\" alt=\"Sunset in the mountains\"><div class=\"px-6 py-4\"><div class=\"font-bold text-xl mb-2\">Hello</div><p class=\"text-grey-darker text-base\">Welcome to the Fuzzy Logic Starter template. You're looking at the result of <code>~/src/index.marko</code>. Try editing that file and this page will reload. &#9889;</p></div><div class=\"px-6 py-4\"><span class=\"inline-block bg-grey-lighter rounded-full px-3 py-1 text-sm font-semibold text-grey-darker mr-2\">#tailwind</span><span class=\"inline-block bg-grey-lighter rounded-full px-3 py-1 text-sm font-semibold text-grey-darker mr-2\">#marko</span><span class=\"inline-block bg-grey-lighter rounded-full px-3 py-1 text-sm font-semibold text-grey-darker\">#express</span><span class=\"inline-block bg-grey-lighter rounded-full px-3 py-1 text-sm font-semibold text-grey-darker\">#prismic</span><span class=\"inline-block bg-grey-lighter rounded-full px-3 py-1 text-sm font-semibold text-grey-darker\">#lasso</span></div></div></div><div class=\"w-full md:w-1/2 px-4\"><div class=\"max-w-sm rounded overflow-hidden shadow-lg mx-auto\"><div class=\"px-6 py-4\"><div class=\"font-bold text-xl mb-2\">Components</div><p class=\"text-grey-darker text-base\">Here's a counter component. You click, it counts! It lives in <code>~/src/components/click-count/</code>. Try making it count by 5.</p>");

            click_count_tag({}, out, __component, "26");

            out.w("<h3>Resolving Components</h3><p class=\"text-grey-darker text-base\">If you look at the code for this page, you'll notice we use the <code>&lt;click-count/></code> component, but don't import it from anywhere. That's because when resolving custom tags, Marko <a href=\"http://markojs.com/docs/custom-tags/#discovering-tags\">looks up the directory tree</a> for a <code>components/</code> directory and finds the counter for us!</p></div></div></div></div></div></div>");
          }
        }
    }, out, __component, "0");
}

marko_template._ = marko_renderer(render, {
    ___implicit: true,
    ___type: marko_componentType
  });

marko_template.Component = marko_defineComponent({}, marko_template._);

marko_template.meta = {
    id: "/fuzzy-node-template$1.0.0/src/index.marko",
    tags: [
      "./components/site-header",
      "./components/click-count",
      "./components/site-layout"
    ]
  };
