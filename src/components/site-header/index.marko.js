// Compiled using marko@4.15.1 - DO NOT EDIT
"use strict";

var marko_template = module.exports = require("marko/src/html").t(__filename),
    marko_component = {
        onCreate: function() {
          this.state = {
              active: true
            };
        },
        toggle: function() {
          this.state.active = !this.state.active;
        }
      },
    marko_componentType = "/fuzzy-node-template$1.0.0/src/components/site-header/index.marko",
    components_helpers = require("marko/src/components/helpers"),
    marko_renderer = components_helpers.r,
    marko_defineComponent = components_helpers.c,
    marko_helpers = require("marko/src/runtime/html/helpers"),
    marko_classAttr = marko_helpers.ca;

function render(input, out, __component, component, state) {
  var data = input;

  out.w("<nav class=\"flex items-center justify-between flex-wrap bg-indigo p-6\"><div class=\"flex items-center flex-no-shrink text-white mr-6\"><span class=\"font-semibold text-xl tracking-tight\">Fuzzy Logic Labs</span></div><div class=\"block lg:hidden\"><button class=\"flex items-center px-3 py-2 border rounded text-indigo-lighter border-indigo-light hover:text-white hover:border-white\"><svg class=\"fill-current h-3 w-3\" viewBox=\"0 0 20 20\" xmlns=\"http://www.w3.org/2000/svg\"><title>Menu</title><path d=\"M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z\"></path></svg></button></div><div" +
    marko_classAttr([
      "nav-wrap",
      state.active ? "hidden" : ""
    ]) +
    "><div class=\"text-sm lg:flex-grow\"><a href=\"#responsive-header\" class=\"block mt-4 lg:inline-block lg:mt-0 text-indigo-lighter hover:text-white mr-4\">Docs</a><a href=\"#responsive-header\" class=\"block mt-4 lg:inline-block lg:mt-0 text-indigo-lighter hover:text-white mr-4\">Examples</a><a href=\"#responsive-header\" class=\"block mt-4 lg:inline-block lg:mt-0 text-indigo-lighter hover:text-white\">Blog</a></div><div><a href=\"#\" class=\"inline-block text-sm px-4 py-2 leading-none border rounded text-white border-white hover:border-transparent hover:text-indigo hover:bg-white mt-4 lg:mt-0\">Download</a></div></div></nav>");
}

marko_template._ = marko_renderer(render, {
    ___type: marko_componentType
  }, marko_component);

marko_template.Component = marko_defineComponent(marko_component, marko_template._);

marko_template.meta = {
    deps: [
      {
          type: "css",
          code: ".nav-wrap {\n    @apply w-full block flex-grow;\n\n    @screen lg {\n      @apply flex items-center w-auto;\n    }\n  }",
          virtualPath: "./index.marko.css",
          path: "./index.marko"
        }
    ],
    id: "/fuzzy-node-template$1.0.0/src/components/site-header/index.marko",
    component: "./"
  };
