const purgecss = require("@fullhuman/postcss-purgecss");

// Custom PurgeCSS extractor for Tailwind that allows special characters in
// class names.
//
// https://github.com/FullHuman/purgecss#extractor
class TailwindExtractor {
  static extract(content) {
    return content.match(/[A-Za-z0-9-_:\/]+/g) || [];
  }
}

module.exports = {
  plugins: [
    require("postcss-import"),
    require("tailwindcss")("./tailwind.js"),
    require("postcss-nested"),
    require("autoprefixer")(),
    purgecss({
      content: ["./src/**/*.marko", "./src/**/*.js"],
      extractors: [
        {
          extractor: TailwindExtractor,

          // Specify the file extensions to include when scanning for
          // class names.
          extensions: ["marko", "js"]
        }
      ]
    })
  ]
};
