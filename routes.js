const layout = require("./src/index");

module.exports = app => {
  // Query the site layout with every route
  app.route("*").get((req, res, next) => {
    req.prismic.api.getSingle("menu").then(function(menuContent) {
      // Define the layout content
      res.locals.menuContent = menuContent;
      next();
    });
  });

  /*
   * Preconfigured prismic preview
   */
  app.get("/preview", (req, res) => {
    const { token } = req.query;
    if (token) {
      req.prismic.api
        .previewSession(token, PrismicConfig.linkResolver, "/")
        .then(url => {
          res.redirect(302, url);
        })
        .catch(err => {
          res.status(500).send(`Error 500 in preview: ${err.message}`);
        });
    } else {
      res.send(400, "Missing token from querystring");
    }
  });

  /*
   * Page route
   */
  app.get("/:uid", (req, res, next) => {
    // Store the param uid in a variable
    const uid = req.params.uid;

    // Get a page by its uid
    req.prismic.api
      .getByUID("page", uid)
      .then(pageContent => {
        if (pageContent) {
          res.marko(layout, {
            pageContent
          });
        } else {
          res.status(404).marko(layout, {
            error: "404"
          });
        }
      })
      .catch(error => {
        next(`error when retriving page ${error.message}`);
      });
  });

  /*
   * Homepage route
   */
  app.get("/", (req, res, next) => {
    req.prismic.api
      .getSingle("homepage")
      .then(pageContent => {
        if (pageContent) {
          // Render the marko layout and stream it back to the client
          res.marko(layout, {
            pageContent
          });
        } else {
          res
            .status(404)
            .send(
              "Could not find a homepage document. Make sure you create and publish a homepage document in your repository."
            );
        }
      })
      .catch(error => {
        next(`error when retriving page ${error.message}`);
      });
  });
};
