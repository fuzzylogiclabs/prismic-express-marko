$_mod.def("/fuzzy-node-template$1.0.0/src/components/site-header/index.marko", function(require, exports, module, __filename, __dirname) { // Compiled using marko@4.15.1 - DO NOT EDIT
"use strict";

var marko_template = module.exports = require('/marko$4.15.1/src/vdom'/*"marko/src/vdom"*/).t(),
    marko_component = {
        onCreate: function() {
          this.state = {
              active: true
            };
        },
        toggle: function() {
          this.state.active = !this.state.active;
        }
      },
    components_helpers = require('/marko$4.15.1/src/components/helpers-browser'/*"marko/src/components/helpers"*/),
    marko_registerComponent = components_helpers.rc,
    marko_componentType = marko_registerComponent("/fuzzy-node-template$1.0.0/src/components/site-header/index.marko", function() {
      return module.exports;
    }),
    marko_renderer = components_helpers.r,
    marko_defineComponent = components_helpers.c,
    marko_helpers = require('/marko$4.15.1/src/runtime/vdom/helpers'/*"marko/src/runtime/vdom/helpers"*/),
    marko_classAttr = marko_helpers.ca,
    marko_attrs0 = {
        "class": "flex items-center justify-between flex-wrap bg-indigo p-6"
      },
    marko_createElement = marko_helpers.e,
    marko_const = marko_helpers.const,
    marko_const_nextId = marko_const("fe4fd1"),
    marko_node0 = marko_createElement("DIV", {
        "class": "flex items-center flex-no-shrink text-white mr-6"
      }, "1", null, 1, 0, {
        i: marko_const_nextId()
      })
      .e("SPAN", {
          "class": "font-semibold text-xl tracking-tight"
        }, null, null, 1)
        .t("Fuzzy Logic Labs"),
    marko_attrs1 = {
        "class": "block lg:hidden"
      },
    marko_attrs2 = {
        "class": "flex items-center px-3 py-2 border rounded text-indigo-lighter border-indigo-light hover:text-white hover:border-white"
      },
    marko_node1 = marko_createElement("svg", {
        "class": "fill-current h-3 w-3",
        viewBox: "0 0 20 20",
        xmlns: "http://www.w3.org/2000/svg"
      }, "5", null, 2, 1, {
        i: marko_const_nextId()
      })
      .e("TITLE", null, null, null, 1)
        .t("Menu")
      .e("path", {
          d: "M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"
        }, null, null, 0, 1),
    marko_node2 = marko_createElement("DIV", {
        "class": "text-sm lg:flex-grow"
      }, "9", null, 3, 0, {
        i: marko_const_nextId()
      })
      .e("A", {
          href: "#responsive-header",
          "class": "block mt-4 lg:inline-block lg:mt-0 text-indigo-lighter hover:text-white mr-4"
        }, null, null, 1)
        .t("Docs")
      .e("A", {
          href: "#responsive-header",
          "class": "block mt-4 lg:inline-block lg:mt-0 text-indigo-lighter hover:text-white mr-4"
        }, null, null, 1)
        .t("Examples")
      .e("A", {
          href: "#responsive-header",
          "class": "block mt-4 lg:inline-block lg:mt-0 text-indigo-lighter hover:text-white"
        }, null, null, 1)
        .t("Blog"),
    marko_node3 = marko_createElement("DIV", null, "13", null, 1, 0, {
        i: marko_const_nextId()
      })
      .e("A", {
          href: "#",
          "class": "inline-block text-sm px-4 py-2 leading-none border rounded text-white border-white hover:border-transparent hover:text-indigo hover:bg-white mt-4 lg:mt-0"
        }, null, null, 1)
        .t("Download");

function render(input, out, __component, component, state) {
  var data = input;

  out.e("NAV", marko_attrs0, "0", component, 3)
    .n(marko_node0, component)
    .e("DIV", marko_attrs1, "3", component, 1)
      .e("BUTTON", marko_attrs2, "4", component, 1, 0, {
          onclick: __component.d("click", "toggle", false)
        })
        .n(marko_node1, component)
    .e("DIV", {
        "class": marko_classAttr([
            "nav-wrap",
            state.active ? "hidden" : ""
          ])
      }, "8", component, 2, 4)
      .n(marko_node2, component)
      .n(marko_node3, component);
}

marko_template._ = marko_renderer(render, {
    ___type: marko_componentType
  }, marko_component);

marko_template.Component = marko_defineComponent(marko_component, marko_template._);

});