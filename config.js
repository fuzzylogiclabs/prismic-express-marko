/**
 * Module dependencies.
 */

const express = require("express");
const markoExpress = require("marko/express");
const favicon = require("serve-favicon");
// const logger = require("morgan");
const bodyParser = require("body-parser");
const methodOverride = require("method-override");
const errorHandler = require("errorhandler");
const path = require("path");
const compression = require("compression");
const minify = require("express-minify");

require("marko/node-require"); // Allow Node.js to require and load `.marko` files
require("marko/browser-refresh").enable();
require("lasso/browser-refresh").enable(
  "*.marko *.css *.png *.jpeg *.jpg *.gif *.webp *.svg"
);

const isProduction = process.env.NODE_ENV === "production";

if (isProduction) console.log("Production Mode Enabled");

// Configure lasso to control how JS/CSS/etc is delivered to the browser
require("lasso").configure({
  plugins: [
    "lasso-marko",
    {
      // Add lasso-postcss to compile tailwind
      plugin: "lasso-postcss",
      config: {
        map: isProduction ? false : "inline"
      }
    }
  ],
  bundlingEnabled: isProduction, // Only enable bundling in production
  // minify: isProduction, // Only minify JS and CSS code in production
  minifyJS: isProduction, // Only minify JS code in production
  fingerprintsEnabled: isProduction // Only add fingerprints to URLs in production
});

module.exports = (() => {
  const app = express();

  // all environments
  app.set("port", process.env.PORT || 3000);
  app.use(markoExpress()); //enable res.marko(template, data)
  app.use(favicon("public/favicon.png"));
  // app.use(logger("dev"));
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(methodOverride());
  app.use(compression());
  app.use(
    minify({
      cache: false,
      jsMatch: false,
      cssMatch: /css/
    })
  );
  app.use(express.static(path.join(__dirname, "public"), { maxAge: 31557600 }));
  app.use(require("lasso/middleware").serveStatic({ maxAge: 31557600 }));

  app.use(errorHandler());

  return app;
})();
