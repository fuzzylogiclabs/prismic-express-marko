const Prismic = require("prismic-javascript");
const PrismicDOM = require("prismic-dom");

const PrismicConfig = require("./prismic-configuration");
const app = require("./config");
const routes = require("./routes");

const PORT = app.get("port");

app.listen(PORT, () => {
  process.stdout.write(`Point your browser to: http://localhost:${PORT}\n`);

  // Browser Refresh
  if (process.send) {
    process.send("online");
  }
});

// Middleware to inject prismic context
app.use((req, res, next) => {
  res.locals.ctx = {
    endpoint: PrismicConfig.apiEndpoint,
    linkResolver: PrismicConfig.linkResolver
  };

  // add PrismicDOM in locals to access them in templates.
  res.locals.PrismicDOM = PrismicDOM;

  Prismic.api(PrismicConfig.apiEndpoint, {
    accessToken: PrismicConfig.accessToken,
    req
  })
    .then(api => {
      req.prismic = { api };
      next();
    })
    .catch(error => {
      next(error.message);
    });
});

routes(app);
